﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using NUnit.Framework;
using ServiceStack.Common.Extensions;
using ServiceStack.Common.Web;
using ServiceStack.Text;
using UrlShortener.Service.Dtos;
using UrlShortener.Service.Helpers;
using UrlShortener.Service.Services;

namespace UrlShortener.Service.Tests
{
    [TestFixture]
    public class ServiceTests
    {
        private ShortenUrlService shortenUrlService { get; set; }
        private FetchUrlService fetchUrlService;

        private ShortenUrlServiceRequest shortenUrlServiceRequest;
        private FetchUrlServiceRequest fetchUrlServiceRequest;

        private readonly UrlShortenerStorePersistor _urlShortenerStorePersistor = new UrlShortenerStorePersistor();


        /// <summary>
        /// In the constructor perform a cleaning of the Urls Json file store
        /// so that the test will perform all required operations properly.
        /// </summary>
        public ServiceTests()
        {
            EmptyTheUrlDataStore();
        }

        /// <summary>
        /// Setup the various services and their dependencies to be used. 
        /// </summary>
        [SetUp]
        public void Setup()
        {
            shortenUrlService = new ShortenUrlService
            {
                ShortenUrlServiceResponse = new ShortenUrlServiceResponse(),
                HttpResponse = new HttpResponse(),
                UrlShortenerStorePersistor = _urlShortenerStorePersistor,
                ShortUrlFactory = new ShortUrlFactory()
            };

            fetchUrlService = new FetchUrlService
            {
                FetchUrlServiceResponse = new FetchUrlServiceResponse(),
                HttpResponse = new HttpResponse(),
                UrlShortenerStorePersistor = _urlShortenerStorePersistor
            };
        }


        /// <summary>
        /// This test will perform the following tasks for each of the TestCases:
        /// 
        /// 1. Using the ShortenUrlService will create the short url for the given source based 
        /// to the parameters provided:
        ///     o AllowLetters
        ///     o AllowNumbers
        ///     o ShortUrlLength
        ///     o SourceUrl
        /// 
        /// 2. Using the FetchUrlService will retrieve from the URLs json file store the source Url 
        /// for the generated short url
        /// 
        /// 3. Using the FetchUrlService will retrieve from the URLs json file store the short Url 
        /// for the source url
        /// 
        /// </summary>
        /// <param name="allowLetters"></param>
        /// <param name="allowNumbers"></param>
        /// <param name="shortUrlLength"></param>
        /// <param name="sourceUrl"></param>
        [Test]
        [TestCase(true, true, 10, "http://www.microsoft.com", Description = "Alphanumeric short url of 10 characters")]
        [TestCase(false, true, 5, "http://www.google.com", Description = "Short url consisting of 5 numbers")]
        [TestCase(true, false, 8, "http://www.satorianalytics.com/", Description = "Short url consisting of 8 letters")]
        public void TestShortenUrlHappyPath(bool allowLetters, bool allowNumbers, int shortUrlLength, string sourceUrl)
        {
            #region Invoke the ShortenUrlService to generate the short url records and store them to data store.
            shortenUrlServiceRequest = new ShortenUrlServiceRequest
            {
                AllowLetters = allowLetters,
                AllowNumbers = allowNumbers,
                ShortUrlLength = shortUrlLength,
                SourceUrl = sourceUrl
            };

            var shortenServiceResponse = (HttpResult)shortenUrlService.OnPost(shortenUrlServiceRequest);

            Assert.NotNull(shortenServiceResponse);
            Assert.AreEqual(shortenServiceResponse.StatusCode, HttpStatusCode.Created);
            Assert.That(shortenServiceResponse.Response is ShortenUrlServiceResponse);

            Console.WriteLine("Service response status: {0}", shortenServiceResponse.StatusCode);
            Console.WriteLine("Service Response: {0}", shortenServiceResponse.Response.SerializeAndFormat());
            #endregion

            Console.WriteLine("\n-----\n");

            var shortUrlCreated = ((ShortenUrlServiceResponse)shortenServiceResponse.Response).ShortUrl;
            var originalSourceUrl = ((ShortenUrlServiceResponse)shortenServiceResponse.Response).SourceUrl;

            #region Invoke FetchUrlService to fetch the source url by the short url generated
            Console.WriteLine("Get the SourceUrl (long url) for the ShortUrl: {0}", shortUrlCreated);

            fetchUrlServiceRequest = new FetchUrlServiceRequest
            {
                InputUrl = shortUrlCreated,
                GetUrlType = GetUrlType.Source
            };

            var fetchServiceResponse = fetchUrlService.OnPost(fetchUrlServiceRequest);

            Assert.AreEqual(fetchServiceResponse, originalSourceUrl);
            Console.WriteLine("Service Response: {0}", fetchServiceResponse);
            #endregion

            Console.WriteLine("\n-----\n");

            #region Invoke FetchUrlService to fetch the created short url for the source url
            Console.WriteLine("Get the ShortUrl created for the SourceUrl: {0}", sourceUrl);

            fetchUrlServiceRequest = new FetchUrlServiceRequest
            {
                InputUrl = sourceUrl,
                GetUrlType = GetUrlType.Short
            };

            fetchServiceResponse = fetchUrlService.OnPost(fetchUrlServiceRequest);

            Assert.AreEqual(fetchServiceResponse, shortUrlCreated);
            Console.WriteLine("Service Response: {0}", fetchServiceResponse);
            #endregion
        }

        /// <summary>
        /// This test will perform the following actions:
        /// 1. Invoke the ShortenUrlService to generate shortUrls for a few sourceUrls
        /// 2. Invoke a GET request to the FetchUrlService with not parameters to get
        /// a full list of the contents in the file store - which they should be the ones
        /// created in step 1
        /// </summary>
        [Test]
        public void TestFetchUrlServiceGetRequestHappyPath()
        {
            #region Invoke the ShortenUrlService to generate the short url records and store them to file store.
            var shortenServiceResponse = new HttpResult();
            var totalTestRecords = 5;
            for (var testRecordCounter = 0; testRecordCounter < totalTestRecords; testRecordCounter++)
            {
                shortenUrlServiceRequest = new ShortenUrlServiceRequest { SourceUrl = string.Format("http://wwww.test-url-{0}.gr",testRecordCounter)};
                shortenServiceResponse = (HttpResult)shortenUrlService.OnPost(shortenUrlServiceRequest);
                Assert.AreEqual(shortenServiceResponse.StatusCode, HttpStatusCode.Created);    
            }
            #endregion

            Console.WriteLine("Get all revords from the file store.");

            var fetchServiceResponse = (List<UrlShortenerStoreRecord>)fetchUrlService.OnGet(new FetchUrlServiceRequest());

            Assert.IsNotNull(fetchServiceResponse);
            Assert.IsTrue(fetchServiceResponse.Count == totalTestRecords);
            
            Console.WriteLine("All entries from the Json file store:\n{0}", fetchServiceResponse.SerializeAndFormat());
        }


        #region Herlper methods
        /// <summary>
        /// For the happy path scenarios to run properly the existing records should be removed
        /// from the URLs data store. If not then an "already exists" - bad request response will
        /// be returned to us and we do not want this.
        /// </summary>
        private void EmptyTheUrlDataStore()
        {
            _urlShortenerStorePersistor.UpdateAccountTransactions(new List<UrlShortenerStoreRecord>());
        }
        #endregion

    }
}
