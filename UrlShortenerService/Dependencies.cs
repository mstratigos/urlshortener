﻿using Funq;
using UrlShortener.Service.Dtos;
using UrlShortener.Service.Helpers;
using UrlShortener.Service.Services;

namespace UrlShortener.Service
{
    public class Dependencies
    {
        public Dependencies(Container container)
        {
            container
                .Register(c => new HttpResponse())
                .ReusedWithin(ReuseScope.None);

            container
                .Register(c => new UrlShortenerStorePersistor())
                .ReusedWithin(ReuseScope.None);

            container
                .Register(c => new ShortenUrlServiceResponse())
                .ReusedWithin(ReuseScope.None);

            container
                .Register(c => new FetchUrlServiceResponse())
                .ReusedWithin(ReuseScope.None);

            container
                .Register(c => new ShortUrlFactory())
                .ReusedWithin(ReuseScope.None);


            container.Register(new ShortenUrlService
            {
                ShortenUrlServiceResponse = container.Resolve<ShortenUrlServiceResponse>(),
                HttpResponse = container.Resolve<HttpResponse>(),
                UrlShortenerStorePersistor = container.Resolve<UrlShortenerStorePersistor>(),
                ShortUrlFactory = container.Resolve<ShortUrlFactory>()
            });

            container.Register(new FetchUrlService
            {
                FetchUrlServiceResponse = container.Resolve<FetchUrlServiceResponse>(),
                HttpResponse = container.Resolve<HttpResponse>(),
                UrlShortenerStorePersistor = container.Resolve<UrlShortenerStorePersistor>()
            });


        }
    }
}