﻿using System.Collections.Generic;
using System.IO;
using log4net;
using Newtonsoft.Json;
using UrlShortener.Service.Dtos;

namespace UrlShortener.Service.Helpers
{
    /// <summary>
    /// This class is responsible for the json file data storage handling
    /// </summary>
    public class UrlShortenerStorePersistor
    {
        private readonly ILog _logger = LogManager.GetLogger(typeof(UrlShortenerStorePersistor));
		private readonly string _jsonUrlShortenerStoreFile = Properties.Settings.Default.UrlShortenerStorePath;

	
		/// <summary>
		/// Reads the contents of the Json urls store file, deserializes and returns
		/// a list of urls store records. If the file does not exist or has not been created
		/// yet, log this fact and return an empty records list.
		/// </summary>
		public virtual List<UrlShortenerStoreRecord> ReadRecords()
		{
            if (!File.Exists(_jsonUrlShortenerStoreFile))
			{
                _logger.WarnFormat("The json store file '{0}' does not exist.", _jsonUrlShortenerStoreFile);
				return new List<UrlShortenerStoreRecord>();
			}


            var accountFileContent = File.ReadAllText(_jsonUrlShortenerStoreFile);
			var transactionsRead = ServiceStack.Text.JsonSerializer.DeserializeFromString<List<UrlShortenerStoreRecord>>(accountFileContent);

			return transactionsRead;
		}

		/// <summary>
        /// Replaces the json store file with the updated <see cref="urlShortenerStoreRecords"/> list.
		/// </summary>
        /// <param name="urlShortenerStoreRecords"></param>
        public virtual bool UpdateAccountTransactions(List<UrlShortenerStoreRecord> urlShortenerStoreRecords)
		{
            var completeUrlShortenerStoreJson = JsonConvert.SerializeObject(urlShortenerStoreRecords.ToArray(), Formatting.Indented);
            File.WriteAllText(_jsonUrlShortenerStoreFile, completeUrlShortenerStoreJson);

			return true;
		} 
    }
}