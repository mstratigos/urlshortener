﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UrlShortener.Service.Helpers
{
    /// <summary>
    /// Factory to create short urls. 
    /// </summary>
    public class ShortUrlFactory
    {
        public string Create(string sourceUrl, int shortUrlLength, ShortUrlStringType shortUrlStringType)
        {
            var shortUrl = string.Empty;

            // strip url from possible leading "http://", it will be re-added to the 
            // short url.
            sourceUrl = sourceUrl.Replace("http://", string.Empty);
            shortUrl = string.Concat("http://", CreateRandomString(shortUrlLength, shortUrlStringType));

            return shortUrl;
        }

        internal protected virtual string CreateRandomString(int size, ShortUrlStringType shortUrlStringType)
        {
            var random = new Random();

            const string alphaCharacters = "abcdefghijklmnopqrstuvwxyz";
            const string numericCharacters = "0123456789";
            var characterSet = string.Empty;

            switch (shortUrlStringType)
            {
                case ShortUrlStringType.AlphaOnly:
                    characterSet = alphaCharacters;
                    break;
                case ShortUrlStringType.NumericOnly:
                    characterSet = numericCharacters;
                    break;
                case ShortUrlStringType.Alphanumeric:
                    characterSet = string.Concat(alphaCharacters, numericCharacters);
                    break;
            }

            var chars = Enumerable.Range(0, size)
                                   .Select(x => characterSet[random.Next(0, characterSet.Length)]);
            return new string(chars.ToArray());
        }

        /// <summary>
        /// An alternative to random string for shorting a url.. Get
        /// its Hash and convert to Hex.
        /// </summary>
        /// <param name="sourceUrl"></param>
        /// <returns></returns>
        internal protected virtual string CreateHashHexRepresentation(string sourceUrl)
        {
            var hashCode = String.Format("{0:X}", sourceUrl.GetHashCode());
            return hashCode;
        }

        /// <summary>
        /// Enum to determine the character set that the new string will be
        /// consisted of.
        /// </summary>
        public enum ShortUrlStringType
        {
            Alphanumeric,
            AlphaOnly,
            NumericOnly
        }
    }
}