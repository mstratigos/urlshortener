﻿using System.Net;
using ServiceStack.Common.Web;

namespace UrlShortener.Service.Helpers
{
    public class HttpResponse
    {
        public HttpResult CreateResponse(HttpStatusCode code, string errorcode, string message)
        {
            var res = new HttpResult { StatusCode = code };
            
            if (string.IsNullOrEmpty(errorcode) || string.IsNullOrEmpty(message)) return res;

            res.Headers["UrlShortener-Error"] = errorcode;
            res.Headers["UrlShortener-Error-Message"] = message;
            return res;
        }        
    }
}