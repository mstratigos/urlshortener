﻿using System;
using System.Net;
using Funq;
using ServiceStack.WebHost.Endpoints;
using UrlShortener.Service.Dtos;

namespace UrlShortener.Service
{
    public class ServiceAppHost : AppHostBase
    {
        protected virtual Dependencies Dependencies { get; set; }

        public ServiceAppHost() : base("UrlShortener Service", typeof(ServiceAppHost).Assembly) { }

        /// <summary>
        /// Configure the service behaviour
        /// </summary>
        /// <param name="container">DI container</param>
        public override void Configure(Container container)
        {
            ConfigureRoutes();
            ConfigureDependancies(container);
            ConfigureDataAccess();
            ConfigureErrorStatus();
        }

        /// <summary>
        /// Adds a filter to return 404 when the service returns null
        /// </summary>
        private void ConfigureErrorStatus()
        {
            // On null return NotFound
            ResponseFilters.Add((request, response, obj) =>
            {
                if (obj == null)
                    response.StatusCode = (int)HttpStatusCode.NotFound;
            });
        }

        /// <summary>
        /// Configures transactions and sessions with DataAccess
        /// </summary>
        private void ConfigureDataAccess()
        {
            // Not for this project :p
        }

        /// <summary>
        /// Configure DI container
        /// </summary>
        /// <param name="container"></param>
        public void ConfigureDependancies(Container container)
        {
            Dependencies = new Dependencies(container);   
        }

        /// <summary>
        ///  Configure the routes the service will listen to.
        /// </summary>
        private void ConfigureRoutes()
        {

            Routes
                .Add<ShortenUrlServiceRequest>("/shortenUrl/", "POST")
                .Add<FetchUrlServiceRequest>("/fetchUrl", "GET")
                //.Add<FetchUrlServiceRequest>("/fetchUrl/{GetUrlType}/{InputUrl}", "GET")
                .Add<FetchUrlServiceRequest>("/fetchUrl", "POST")
            ;
        }

        /// <summary>
        /// Start the service setting it up. 
        /// 
        /// Used by WebActivator.PostApplicationStartMethod
        /// </summary>
        public static void Start()
        {
            new ServiceAppHost().Init();
        }
    }
}