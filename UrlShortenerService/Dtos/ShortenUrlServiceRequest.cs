﻿using System;

namespace UrlShortener.Service.Dtos
{
    public class ShortenUrlServiceRequest
    {
        /// <summary>
        /// This is the Long Url to get short-ed.
        /// </summary>
        public virtual string SourceUrl { get; set; }

        /// <summary>
        /// Setting to true the shorting algorithm will include letters
        /// (alpha) when creating the short Url.
        /// </summary>
        public virtual bool AllowLetters { get; set; }

        /// <summary>
        /// Setting to true the shorting algorithm will include numbers
        /// when creating the short Url.
        /// </summary>
        public virtual bool AllowNumbers { get; set; }

        /// <summary>
        /// Number of characters the short url will consist of.
        /// </summary>
        public int ShortUrlLength { get; set; }
    }
}