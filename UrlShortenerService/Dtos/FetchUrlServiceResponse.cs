﻿namespace UrlShortener.Service.Dtos
{
    public class FetchUrlServiceResponse
    {
        public virtual string SourceUrl { get; set; }
        public virtual string ShortUrl { get; set; }
    }
}