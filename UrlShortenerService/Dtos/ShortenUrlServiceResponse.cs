﻿namespace UrlShortener.Service.Dtos
{
    public class ShortenUrlServiceResponse
    {
        /// <summary>
        /// The source (long) url.
        /// </summary>
        public virtual string SourceUrl { get; set; }

        /// <summary>
        /// The produced short url.
        /// </summary>
        public virtual string ShortUrl { get; set; }
    }
}