﻿namespace UrlShortener.Service.Dtos
{
    public class FetchUrlServiceRequest
    {
        /// <summary>
        /// The input url is the url string the client should provide depending on the 
        /// GetUrlType url parameter required to be returned: Source, Short:
        /// o If "Source" is required to be returned then the InputUrl is the ShortUrl.
        /// o If "Short" is required to be returned then the InputUrl is the SourceUrl.
        /// </summary>
        public virtual string InputUrl { get; set; }

        public virtual GetUrlType? GetUrlType { get; set; }
    }

    public enum GetUrlType
    {
        Source,
        Short
    }
}