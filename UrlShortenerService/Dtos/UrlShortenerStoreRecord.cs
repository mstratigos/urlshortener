﻿using System;

namespace UrlShortener.Service.Dtos
{
    public class UrlShortenerStoreRecord
    {
        public virtual Guid Id { get; set; }
        public virtual string CreationTimestamp { get; set; }
        public virtual string SourceUrl { get; set; }
        public virtual string ShortUrl { get; set; }
    }
}