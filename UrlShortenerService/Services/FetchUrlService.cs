﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using log4net;
using ServiceStack.ServiceInterface;
using UrlShortener.Service.Dtos;
using UrlShortener.Service.Helpers;

namespace UrlShortener.Service.Services
{
    public class FetchUrlService : RestServiceBase<FetchUrlServiceRequest>
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(FetchUrlService));
        internal protected FetchUrlServiceResponse FetchUrlServiceResponse { get; set; }
        internal protected HttpResponse HttpResponse { get; set; }
        internal protected UrlShortenerStorePersistor UrlShortenerStorePersistor { get; set; }

        public override object OnGet(FetchUrlServiceRequest request)
        {
            Log.Info("FetchSourceServiceResponse new GET request.");
            try
            {
                ValidateRequest(request);
                var allRecords = FetchUrlRecord();

                // If no records were retrieved return 404 - Not found error.
                if (!allRecords.Any())
                    return HttpResponse.CreateResponse(HttpStatusCode.NotFound, "Service Error", "No records found in the data store.");

                return allRecords;

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return HttpResponse.CreateResponse(HttpStatusCode.InternalServerError, "Service Error", ex.Message);
            }
        }

        public override object OnPost(FetchUrlServiceRequest request)
        {
            Log.Info("FetchSourceServiceResponse new POST request.");
            try
            {
                ValidateRequest(request);
                var matchingRecords = FetchUrlRecord(request.GetUrlType, request.InputUrl);

                // If no records were retrieved return 404 - Not found error.
                if (!matchingRecords.Any())
                    return HttpResponse.CreateResponse(HttpStatusCode.NotFound, "Service Error",
                        string.Format("No records found in the data store matching the criteria provided: return type '{0}' for input url '{1}'",
                        request.GetUrlType, request.InputUrl));

                // TODO: Validation and handling of multiple records! THERE SHOULD NOT be any, based to the ShortenUrlService implementation!

                // Return the requested url type (long/source or short)

                var requestedUrl = string.Empty;
                switch (request.GetUrlType)
                {
                    case GetUrlType.Short:
                        requestedUrl= matchingRecords.FirstOrDefault().ShortUrl;
                        break;
                    case GetUrlType.Source:
                        requestedUrl = matchingRecords.FirstOrDefault().SourceUrl;
                        break;
                }

                return requestedUrl;
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return HttpResponse.CreateResponse(HttpStatusCode.InternalServerError, "Service Error", ex.Message);
            }
        }

        /// <summary>
        /// Method to load all records from the database and return the required ones based
        /// on the <param name="getUrlType"/> and <param name="inputUrl"/> provided. Both these
        /// parameneters are optional and if not provided the list of all records is returned.
        /// </summary>
        /// <param name="getUrlType">optional</param>
        /// <param name="inputUrl">optional</param>
        /// <returns></returns>
        internal protected virtual IList<UrlShortenerStoreRecord> FetchUrlRecord(GetUrlType? getUrlType = null, string inputUrl = null)
        {
            var urlShortenerStoreRecords = UrlShortenerStorePersistor.ReadRecords();
            switch (getUrlType)
            {
                case GetUrlType.Source:
                    // Get records based on the InputUrl being the ShortUrl
                    return urlShortenerStoreRecords.Where(x => x.ShortUrl == inputUrl).ToList();
                case GetUrlType.Short:
                    // Get records based on the InputUrl being the SourceUrl
                    return urlShortenerStoreRecords.Where(x => x.SourceUrl == inputUrl).ToList();
                default:
                    // Get all records
                    return urlShortenerStoreRecords;
            }
        }


        /// <summary>
        /// Perform necessary validations logging warnings or errors as appropriate.
        /// </summary>
        /// <param name="request"></param>
        internal protected virtual void ValidateRequest(FetchUrlServiceRequest request)
        {
            if (request.GetUrlType == null)
            {
                Log.Warn("No GetUrlType was specified. Retrieving all records.");
                return;
            }

            if (string.IsNullOrWhiteSpace(request.GetUrlType.ToString())
                || (!Enum.IsDefined(typeof(GetUrlType), request.GetUrlType)))
                throw new ArgumentException("A valid GetUrlType must be provided. Valid values are: \"Source\", \"Short\" and \"All\".");

            if (string.IsNullOrWhiteSpace(request.InputUrl) && request.GetUrlType != null)
            {
                switch (request.GetUrlType)
                {
                    case GetUrlType.Source:
                        throw new ArgumentException("InputUrl cannot be blank and must be the ShortUrl for which the SourceUrl is required to be returned ");
                    case GetUrlType.Short:
                        throw new ArgumentException("InputUrl cannot be blank and must be the SourceUrl for which the ShortUrl is required to be returned ");
                }
            }
        }

    }
}