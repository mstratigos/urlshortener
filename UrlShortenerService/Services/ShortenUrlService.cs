﻿using System;
using System.Linq;
using System.Net;
using log4net;
using ServiceStack.Common.Web;
using ServiceStack.ServiceInterface;
using UrlShortener.Service.Dtos;
using UrlShortener.Service.Helpers;

namespace UrlShortener.Service.Services
{
    public class ShortenUrlService : RestServiceBase<ShortenUrlServiceRequest>
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(ShortenUrlService));
        internal protected ShortenUrlServiceResponse ShortenUrlServiceResponse { get; set; }
        internal protected HttpResponse HttpResponse { get; set; }
        internal protected UrlShortenerStorePersistor UrlShortenerStorePersistor { get; set; }
        internal protected ShortUrlFactory ShortUrlFactory { get; set; }

        private readonly int _minShortUrlLength = Properties.Settings.Default.MinShortUrlLength;

        public override object OnPost(ShortenUrlServiceRequest request)
        {
            Log.Info("ShortenUrlService new POST request.");
            try
            {
                // Validate post request
                ValidateRequest(request);

                // Set the default shortUrlStringType. Will be overwritten if the relevant flags 
                // have been set in the request object.
                var shortUrlStringType = ShortUrlFactory.ShortUrlStringType.AlphaOnly;

                if(request.AllowLetters && !request.AllowNumbers)
                    shortUrlStringType = ShortUrlFactory.ShortUrlStringType.AlphaOnly;
                if (!request.AllowLetters && request.AllowNumbers)
                    shortUrlStringType = ShortUrlFactory.ShortUrlStringType.NumericOnly;
                if (request.AllowLetters && request.AllowNumbers)
                    shortUrlStringType = ShortUrlFactory.ShortUrlStringType.Alphanumeric;

                // Set the shortUrlLength: If the requested in the request object is 
                // smaller than the minimum allowed (set in configuration file) or 0
                // then use the minimum.
                var shortUrlLength = request.ShortUrlLength >= _minShortUrlLength
                    ? request.ShortUrlLength
                    : _minShortUrlLength;

                // Use the ShortUrlFactory passing it the required parameters to create the short Url.
                var shortUrl = 
                    ShortUrlFactory
                    .Create(request.SourceUrl, shortUrlLength, shortUrlStringType);

                // Build the response object the service will return
                ShortenUrlServiceResponse = new ShortenUrlServiceResponse
                {
                    SourceUrl = request.SourceUrl,
                    ShortUrl = shortUrl
                };

                #region Update data storage
                // TODO: Separate Store handling logic from here - not for now.

                // Load all storage records and check if there is already one for the requested sourceUrl
                // If so return an error response with appropriate message. Else add the new record to the
                // complete records list and save back to store.
                var completeUrlShortenerStore = UrlShortenerStorePersistor.ReadRecords();
                var sourceUrlAlreadyShortened = completeUrlShortenerStore.FirstOrDefault(x => x.SourceUrl == request.SourceUrl);

                if (sourceUrlAlreadyShortened != null)
                {
                    var sourceUrlAlreadyShortenedMessage = 
                        string.Format("SourceUrl '{0}' already shorted to '{1}' on the '{2}'.",
                        sourceUrlAlreadyShortened.SourceUrl, sourceUrlAlreadyShortened.ShortUrl,
                        sourceUrlAlreadyShortened.CreationTimestamp);
                    return HttpResponse.CreateResponse(HttpStatusCode.BadRequest, "Service Error", sourceUrlAlreadyShortenedMessage);
                }

                // Based to the response object, build the record to save to storage. 
                var urlShortenerStoreRecord = new UrlShortenerStoreRecord
                {
                    Id = Guid.NewGuid(),
                    CreationTimestamp = DateTime.Now.ToString("yyyy-mm-dd hh:mm:ss"),
                    SourceUrl = ShortenUrlServiceResponse.SourceUrl,
                    ShortUrl = ShortenUrlServiceResponse.ShortUrl
                };

                // Add record to the list of records
                completeUrlShortenerStore.Add(urlShortenerStoreRecord);
                // Update store.
                UrlShortenerStorePersistor.UpdateAccountTransactions(completeUrlShortenerStore);
                #endregion

            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
                return HttpResponse.CreateResponse(HttpStatusCode.InternalServerError, "Service Error", ex.Message);
            }

            // return successful service response: 201 and the ShortenUrlServiceResponse
            return new HttpResult { StatusCode = HttpStatusCode.Created, Response = ShortenUrlServiceResponse };
        }

        /// <summary>
        /// Perform necessary validations logging warnings or errors as appropriate.
        /// </summary>
        /// <param name="request"></param>
        private void ValidateRequest(ShortenUrlServiceRequest request)
        {
            if (string.IsNullOrWhiteSpace(request.SourceUrl))
                throw new ArgumentException("SourceUrl cannot be blank.");

            if (!request.AllowLetters && !request.AllowNumbers)
                Log.WarnFormat("Neither AllowLetters or AllowNumbers specified. Default will be used.");

            if (request.ShortUrlLength == 0 || request.ShortUrlLength < _minShortUrlLength)
                Log.WarnFormat("ShortUrlLength requested '{0}' is invalid. Using minimum '{1}'",
                    request.ShortUrlLength, _minShortUrlLength);
        }
    }
}